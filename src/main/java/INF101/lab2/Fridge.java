package INF101.lab2;


// import static org.junit.jupiter.api.Assertions.*;
// import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
public class Fridge implements IFridge {

    ArrayList<FridgeItem> FridgeItems = new ArrayList<FridgeItem>();

    int maxFridgeItems = 20;

    public int totalSize(){
        return maxFridgeItems;
    }

    public int nItemsInFridge(){
        return FridgeItems.size();
    }

    public void emptyFridge(){
        FridgeItems.clear();
    }

    public void takeOut(FridgeItem item){
        Iterator <FridgeItem> iter = FridgeItems.iterator();
        iter.next();
        FridgeItems.remove(item);

    }

    public List<FridgeItem> removeExpiredFood(){
        ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for (FridgeItem item : FridgeItems){
            if (item.hasExpired()){
                expiredItems.add(item);
            }
        }
        for (FridgeItem exItem : expiredItems){
            takeOut(exItem);
        }
        return expiredItems;
    }

    public boolean placeIn(FridgeItem item){
        if (nItemsInFridge() < totalSize()){
            FridgeItems.add(item);
            return true;
        } else {
            return false;
        }
    }
}
